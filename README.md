# git-for-network-engineers-ansible-demo

This repo is based upon a simple NetDevOps demo to deploy SNMP to a Cisco IOS device via Ansible

## Table of Contents
- [Usage](#usage)
- [Requirements](#requirements)
- [Contributing](#contributing)

## Usage
This demo assumes one or more Cisco IOS devices are configure for public-key authentication.

```bash
ansible-playbook playbook.yml --private-key=~/.ssh/id_rsa -i hosts.yml
```

With Docker
```angular2html
docker login registry.gitlab.com
docker pull registry.gitlab.com/sms-priv/containers/docker-ansible
docker image tag registry.gitlab.com/sms-priv/containers/docker-ansible docker-ansible

docker run -it --rm -v $(pwd)/pkey.pem:/root/.ssh/id_rsa:ro -v $(pwd):/data docker-ansible ansible-playbook -i hosts.yml playbook.yml
```

## Requirements
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Contributing
Code quality and security will be validated before merge requests are accepted.

### Tools
These tools are used to ensure validation and standardization of Terraform deployments

#### Must be installed
- [pre-commit](https://github.com/gruntwork-io/pre-commit/releases)
- [ansible-lint](https://github.com/terraform-docs/terraform-docs)

NOTE: `pre-commit` and `ansible-lint` require [Python](https://www.python.org/) 

For more information see - [pre-commit and ansible](https://blog.emedeiros.me/archives/2020/03/20/ansible-best-practices-checker-with-pre-commit-and-ansible-lint.html)

### To submit a merge request
```bash
git checkout -b <branch name>
pre-commit autoupdate
pre-commit run -a
git commit -a -m 'Add new feature'
git push origin <branch name>
```
Optionally run the following to automate the execution of pre-commit on every git commit.
```bash
pre-commit install
```
In the output, GitLab prompts you with a direct link for creating a merge request:
```bash
...
remote: To create a merge request for docs-new-merge-request, visit:
remote:   https://gitlab-instance.com/my-group/my-project/merge_requests/new?merge_request%5Bsource_branch%5D=my-new-branch
```

# License
Copyright (c) 2020 [SMS Data Products](https://www.sms.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
